<?php
// source: persons.latte

use Latte\Runtime as LR;

class Template0910d44f39 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['o'])) trigger_error('Variable $o overwritten in foreach on line 23');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Persons list<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

<div class="container">
	<a class="btn btn-success" href="<?php
		echo $router->pathFor("newPerson");
?>">
		Add new person
	</a>
</div>

<div class="container mt-5">
	<table class="table table-stripped table-hover">
		<tr>
			<th>First name</th>
			<th>Last name</th>
			<th>Height</th>
			<th>Gender</th>
			<th>Edit</th>
		</tr>

<?php
		$iterations = 0;
		foreach ($osoby as $o) {
?>
			<tr>
				<td><?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 25 */ ?></td>
				<td><?php echo LR\Filters::escapeHtmlText($o['last_name']) /* line 26 */ ?></td>
				<td><?php echo LR\Filters::escapeHtmlText($o['height']) /* line 27 */ ?></td>
				<td><?php echo LR\Filters::escapeHtmlText($o['gender']) /* line 28 */ ?></td>
				<!-- Edit button -->
	            <td>
	                <a href="<?php
			echo $router->pathFor("persons_update");
			?>?id_person=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($o['id_person'])) /* line 31 */ ?>">
	                    <button class="btn-sm btn-primary">
	                        <span class="fa fa-edit"></span>
	                    </button>
	                </a>
	            </td>
			</tr>
<?php
			$iterations++;
		}
?>
	</table>
</div>


<?php
	}

}

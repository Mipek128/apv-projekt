<?php
// source: meetings.latte

use Latte\Runtime as LR;

class Templateec054295b2 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['s'])) trigger_error('Variable $s overwritten in foreach on line 24');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Meetings list<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <div class="container">
        <a class="btn btn-success" href="<?php
		echo $router->pathFor("newMeeting");
?>">
            Add new meeting
        </a>
    </div>


    <div class="container mt-5">
        <table class="table table-stripped table-hover">
            <tr>
                <th>start</th>
                <th>description	</th>
                <th>duration</th>
                <th>id_location</th>

            </tr>

<?php
		$iterations = 0;
		foreach ($schuzky as $s) {
?>
                <tr>
                    <td><?php echo LR\Filters::escapeHtmlText($s['id_meeting']) /* line 26 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($s['start']) /* line 27 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($s['description']) /* line 28 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($s['duration']) /* line 29 */ ?></td>
                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
